import { ILinks } from "./ILinks"

export interface IAccountTransactionsResponse {
  transactions: Array<ITransaction>,
  links: ILinks
}

export interface ITransaction {
  id: string,
  label: string,
  sign: string,
  amount: number,
  currency: string
}

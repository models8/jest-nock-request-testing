import { axiosBridgeIo } from "./config/axios"
import { BRIDGEIO_API } from "./config/api"
import { IAccount, IAccountsResponse } from "./definitions/IAccountsResponse"
import { AxiosError, AxiosResponse } from "axios"
import { Result } from "../../../utility/Result"

/**
 * Return some bridgeIo accounts through specific pagination
 * @param page? - Target page
 * @returns The bridgeIo api accounts response
 * */
const getAccounts = async (page : number = 1) : Promise<Result<IAccountsResponse, string>> => {
  try {
    const axiosAccountsResponse : AxiosResponse<IAccountsResponse> = await axiosBridgeIo
      .get<IAccountsResponse>(BRIDGEIO_API.paths.accounts, { params: { page } })
    return { success: true, value: axiosAccountsResponse.data }
  }
  catch (error: AxiosError|unknown){
    /*TODO: -->  Logging all errors that are generated by the API calls ( maybe in AWS Cloudwatch ) or plug app monitoring system (like APM ) so that developers can be notified of these errors*/
    if (error instanceof AxiosError)
      return { success: false, value: "getAccounts() api call failed" }
    throw error
  }
}

/**
 * Return all bridgeIo accounts through all pagination
 * @returns All bridgeIo api accounts
 * */
const getAllAccounts = async (_getAccounts: typeof getAccounts) : Promise<Result<IAccount[], string>> => {
  const totalAccounts : IAccount[] = []
  let nextPage : number|null = 1
  do {
    const { success, value } : Result<IAccountsResponse, string> = await _getAccounts(nextPage)
    if (!success)
      /*TODO: -->  Logging all errors that are generated by the API calls ( maybe in AWS Cloudwatch ) or plug app monitoring system (like APM ) so that developers can be notified of these errors*/
      return { success: false, value: "getAllAccounts() api call failed" }
    totalAccounts.push(...value.accounts)
    nextPage = value.links.next ? ++nextPage : null
  } while (nextPage)

  return { success: true, value: totalAccounts }
}

export { getAccounts, getAllAccounts }

import { IAccountsResponse } from "../../definitions/IAccountsResponse"
import { Result } from "../../../../../utility/Result"

export const ACCOUNTS_SUCCESS_RESULT : Result<IAccountsResponse, string> = {
  success: true,
  value: {
    "accounts": [
      {
        "acc_number": "0000001",
        "amount": 50,
        "currency": "EUR"
      }
    ],
    "links": {
      "self": "/accounts?page=1",
      "next": "/accounts?page=2"
    }
  }
}

export const ACCOUNTS_FAIL_RESULT : Result<IAccountsResponse, string> = {
  success: false,
  value: "A message"
}

export const ACCOUNTS_SUCCESS_RESULT_END_OF_PAGINATION : Result<IAccountsResponse, string> = {
  success: true,
  value: {
    "accounts": [
      {
        "acc_number": "0000008",
        "amount": 500,
        "currency": "EUR"
      }
    ],
    "links": {
      "self": "/accounts?page=1",
      "next": ""
    }
  }
}

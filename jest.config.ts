import * as path from 'path'

const projectRoot = process.cwd()

export default {
  rootDir: projectRoot,
  coverageProvider: 'v8',
  testEnvironment: 'node',
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  collectCoverage: false,
  modulePathIgnorePatterns: ['global-setup'],
}

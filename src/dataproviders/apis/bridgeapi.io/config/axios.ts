import axios, { AxiosInstance } from "axios"
import { BRIDGEIO_API } from "./api"

export const axiosBridgeIo : AxiosInstance = axios.create({
  baseURL: BRIDGEIO_API.baseUrl,
  headers: { "X-API-KEY": BRIDGEIO_API.apiKey }
})

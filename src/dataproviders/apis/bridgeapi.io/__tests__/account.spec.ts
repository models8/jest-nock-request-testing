import nock, { Scope } from "nock"
import { BRIDGEIO_API } from "../config/api"
import { ACCOUNT_API_RESPONSE } from "./stubs/accountsApiResponse.stub"
import { getAccounts, getAllAccounts } from "../accounts"
import {
  ACCOUNTS_FAIL_RESULT,
  ACCOUNTS_SUCCESS_RESULT,
  ACCOUNTS_SUCCESS_RESULT_END_OF_PAGINATION
} from "./stubs/accountsResultPayload.stub"
import { Result } from "../../../../utility/Result"
import { IAccount, IAccountsResponse } from "../definitions/IAccountsResponse"

describe("#getAccounts()", () => {

  beforeEach(()=> nock.cleanAll)

  it("should perform api call with valid headers", async () => {
    const scope : Scope = nock(BRIDGEIO_API.baseUrl)
      .get(BRIDGEIO_API.paths.accounts)
      .query(true) //ignore queryParams matching
      .matchHeader("X-API-KEY", BRIDGEIO_API.apiKey)
      .reply(200, ACCOUNT_API_RESPONSE)

    await getAccounts()
    scope.done()
  })

  it("should perform api call with valid default pagination number", async () => {
    const scope : Scope = nock(BRIDGEIO_API.baseUrl)
      .get(BRIDGEIO_API.paths.accounts)
      .query({ page: "1" })
      .reply(200, ACCOUNT_API_RESPONSE)

    await getAccounts()
    scope.done()
  })

  it("should perform api call with valid pagination number", async () => {
    const scope : Scope = nock(BRIDGEIO_API.baseUrl)
      .get(BRIDGEIO_API.paths.accounts)
      .query({ page: "3" })
      .reply(200, ACCOUNT_API_RESPONSE)

    await getAccounts(3)
    scope.done()
  })

  it("should return success payload response", async () => {
    const scope : Scope = nock(BRIDGEIO_API.baseUrl)
      .get(BRIDGEIO_API.paths.accounts)
      .query(true) //ignore queryParams matching
      .reply(200, ACCOUNT_API_RESPONSE)

    const result : Result<IAccountsResponse, string> = await getAccounts()
    scope.done()
    expect(result).toMatchSnapshot()
  })

  it("should return failed payload response", async () => {
    const scope : Scope = nock(BRIDGEIO_API.baseUrl)
      .get(BRIDGEIO_API.paths.accounts)
      .query(true) //ignore queryParams matching
      .reply(401)

    const result = await getAccounts()
    scope.done()
    expect(result).toMatchSnapshot()
  })
})

describe("#getAllAccounts()", () =>{

  it("should call getAccounts() function 3 time", async () => {
    const mockGetAccounts = jest.fn()
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT)
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT)
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT_END_OF_PAGINATION)
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT)
    await getAllAccounts(mockGetAccounts)
    expect(mockGetAccounts).toHaveBeenCalledTimes(3)
  })

  it("should call getAccounts() with valid page number", async () => {
    const mockGetAccounts = jest.fn()
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT)
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT_END_OF_PAGINATION)

    await getAllAccounts(mockGetAccounts)
    expect(mockGetAccounts).toHaveBeenNthCalledWith(1, 1)
    expect(mockGetAccounts).toHaveBeenNthCalledWith(2, 2)
  })

  it("should return all accounts", async () => {
    const mockGetAccounts = jest.fn()
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT)
      .mockResolvedValueOnce(ACCOUNTS_SUCCESS_RESULT_END_OF_PAGINATION)
    const result : Result<IAccount[], string> = await getAllAccounts(mockGetAccounts)
    expect(result).toMatchSnapshot()
  })

  it("should return a fail result", async () => {
    const mockGetAccounts = jest.fn().mockResolvedValueOnce(ACCOUNTS_FAIL_RESULT)
    const result : Result<IAccount[], string> = await getAllAccounts(mockGetAccounts)
    expect(result).toMatchSnapshot()
  })
})
